import Pokemon from "./Pokemon.ts";

interface Boxe {
    id: number|null|undefined
    name:string
    trainerId:number
    pokemons:Array<Pokemon>
}

export default Boxe