import ConnectedHeader from "../Header/ConnectedHeader.tsx";
import PrivateRoute from "../../Router/PrivateRoute.tsx";
import {useEffect, useState} from "react";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import Boxe from "../../Entity/Interfaces/Boxe.ts";
import {Link, useLocation, useNavigate} from "react-router-dom";
import CustomOption from "../../Entity/Interfaces/CustomOption.ts";
import {IoIosAdd} from "react-icons/io";
import PropMessage from "../Message/PropMessage.tsx";
import Pokemon from "../../Entity/Interfaces/Pokemon.ts";
import GenderTypeCode from "../../Entity/GenderTypeCode.ts";
import {IoArrowBack, IoPencil} from "react-icons/io5";
import Footer from "../Footer/Footer.tsx";
import Message from "../Message/Message.tsx";

function DetailBoxe() {

    const navigate = useNavigate()
    const location = useLocation()
    const [pokemon, setPokemons] = useState<Array<Pokemon>>([])

    let customOptions: CustomOption = {
        state: {
            message: "",
            polarity: ""
        }
    }

    const boxId = location.state.box.id
    const trainerId = localStorage.getItem("trainer-id")
    useEffect(() => {

        fetch(`http://localhost:8000/trainers/${trainerId}/boxes/${boxId}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                },
            })
            .then(async (response) => {
                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                const data: Boxe = await response.json();
                setPokemons(data.pokemons)
            })
            .catch((error) => {
                customOptions.state.polarity = "negative"
                if (error.codeStatus === 401)
                    customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                else
                    customOptions.state.message = "Aucune boîte vous appartenant n'a été retrouvée."

            })

    })

    const style = {
        display: "flex",
        justifyContent: "center"
    }

    return(
        <PrivateRoute defaultLocation="sign-in">
            <div>
                <ConnectedHeader/>
                <div style={style}>
                    <div
                        className="bg-blue-950 border-t-4 border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3">
                        <p className="text-white text-center">Boîte n°: { location.state.box.id }</p>
                    </div>
                    <div className="mt-5 ml-5">
                        <button type="button"
                                className="text-white bg-gray-900 hover:bg-green-500 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                onClick={
                                    () => {
                                        navigate("/add-pokemon")
                                    }
                                }>
                            <IoIosAdd className="w-14 h-14"/>
                        </button>
                        <button type="button"
                                className="text-white bg-gray-900 ml-2 hover:bg-green-500 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                onClick={
                                    () => {
                                        navigate("/list-boxes")
                                    }
                                }>
                            <IoArrowBack className="w-14 h-14"/>
                        </button>
                        <button type="button"
                                className="text-white bg-gray-900 ml-2 hover:bg-green-500 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                onClick={
                                    () => {
                                        navigate("/list-boxes")
                                    }
                                }>
                            <IoPencil className="w-14 h-14"/>
                        </button>
                    </div>
                </div>

                <div className="grid grid-cols-4 gap-4 mt-10 ml-3 mr-5">
                    {
                        pokemon.map(pokemon => {
                            return (
                                <Link to={ "/detail-pokemon"} state={{
                                    pokemonId: pokemon.id
                                }}>
                                    <div
                                        className="bg-white border-t-4 shadow-xl border-blue-900 flex flex-col w-10/12 hover:bg-blue-950 hover:border-green-300 hover:text-white">
                                        <div className="ml-5 mt-5 w-2/3 h-full">
                                            <p className="w-fit"><strong>Espèce :</strong> {pokemon.species}</p>
                                            <p className="w-fit"><strong>Name :</strong> {pokemon.name}</p>
                                            <p className="w-fit"><strong>Niveau :</strong> {pokemon.level}</p>
                                            <p className="w-full"><strong>Genre
                                                :</strong> {pokemon.genderTypeCode === GenderTypeCode.MALE ? "Male" : pokemon.genderTypeCode === GenderTypeCode.FEMALE ? "Female" : "Non défnie"}
                                            </p>
                                            <p className="w-fit"><strong>Chromatique
                                                :</strong> {pokemon.isShiny ? "Oui" : "Non"}</p>
                                        </div>
                                        <button type="button"
                                                className="w-6/12 h-9 mt-5 mb-3 mr-auto ml-auto text-white bg-gray-900 hover:bg-green-500  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                                onClick={
                                                    () => {
                                                        /*const options = {
                                                            state: {
                                                                box: boxe,
                                                                trainerId: localStorage.getItem("trainer-id")
                                                            },
                                                            replace: true
                                                        }*/
                                                        navigate("/detail-pokemon")
                                                    }
                                                }>Déplacer
                                        </button>
                                        <button type="button"
                                                className="w-6/12 h-9 mr-auto ml-auto mb-5 text-white bg-red-500 hover:bg-red-800  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800" onClick={() => {
                                                const pokemonId = pokemon.id
                                                    fetch(`http://localhost:8000/pokemons/${pokemonId}`,
                                                {
                                                    method: "DELETE",
                                                    headers: {
                                                        "Content-Type": "application/json",
                                                        "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                                                    },
                                                })
                                                .then(async (response) => {
                                                    if (!response.ok)
                                                        throw new HttpErrorResponse(response.status)
                                                    else {
                                                        let options = {
                                                            state: {
                                                                message: "Votre pokemon a été supprimé avec succès !",
                                                                polarity: "positive"
                                                            },
                                                            replace: false
                                                        }
                                                        navigate("/list-boxes", options)
                                                    }
                                                })
                                                .catch((error) => {
                                                    customOptions.state.polarity = "negative"
                                                    if (error.codeStatus === 401)
                                                        customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                                                    else
                                                        customOptions.state.message = "Vous n'êtes pas autorisé à supprimer ce pokemon."

                                                })

                                        }}>
                                            Supprimer
                                        </button>
                                    </div>
                                </Link>


                            )
                        })
                    }
                </div>


                <div className="w-1/2 ml-auto mr-auto mt-3">
                    <PropMessage state={customOptions.state}/>
                </div>
                <div className="w-1/2 ml-auto mr-auto mt-3">
                    <Message/>
                </div>
            </div>
            <Footer/>
        </PrivateRoute>
    )
}

export default DetailBoxe