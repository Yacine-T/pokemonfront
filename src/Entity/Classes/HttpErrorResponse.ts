class HttpErrorResponse extends Error {

    codeResponse:number
    constructor(codeResponse: number) {
        super();
        this.codeResponse = codeResponse
    }
}

export default HttpErrorResponse