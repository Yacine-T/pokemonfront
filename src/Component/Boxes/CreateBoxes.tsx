import ConnectedHeader from "../Header/ConnectedHeader.tsx";
import PrivateRoute from "../../Router/PrivateRoute.tsx";
import Footer from "../Footer/Footer.tsx";
import {ChangeEvent, FormEvent, useState} from "react";
import Boxe from "../../Entity/Interfaces/Boxe.ts";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import {useNavigate} from "react-router-dom";
import CustomOption from "../../Entity/Interfaces/CustomOption.ts";
import PropMessage from "../Message/PropMessage.tsx";

function CreateBoxes() {
    const navigate = useNavigate()
    const boxeInitializer: Boxe = {
        id: null,
        name: "",
        trainerId: -1
    }
    const [boxe, setBoxe] = useState(boxeInitializer)

    let customOptions: CustomOption = {
        state: {
            message: "",
            polarity: ""
        }
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        let route: string = ""
        window.history.replaceState(null, '')
        
        let options = {
            state : {
                message: "",
                polarity: "",
            },
            replace: false
        }
        const trainerId = localStorage.getItem("trainer-id")

        fetch(`http://localhost:8000/trainers/${trainerId}/boxes`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("user-token")}`
            },
            body: JSON.stringify(boxe)
        })
            .then( (response) => {
                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                else
                {
                    route = "/list-boxes"
                    options.state.polarity = "positive"
                    options.state.message = "Félicitation, vous disposez d'une nouvelle boîte pour vos pokemon !"
                    navigate(route, options)
                }
            })
            .catch( (error) => {
                route = "/create-boxes"
                options.state.polarity = "negative"
                if (error.codeResponse === 400)
                {
                    options.state.message = "Un champs n'est pas remplis. Merci de remplir tous les champs avant d'essayer de vous créer une boîte."
                }
                else if(error.codeResponse === 401)
                {
                    options.state.message = "Vous ne pouvez pas créer de boîte car vous n'avez pas été authentifié. Merci de vous authentifié."
                }
                else {
                    options.state.message = "Vous n'êtes pas autorisé à créer une boîte."
                }
                navigate(route, options)
            })
    }

    return (
      <PrivateRoute defaultLocation="/sign-in">
          <div>
              <ConnectedHeader/>
              <div>
                  <div className="bg-blue-950 border-t-4 border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3 mb-4 ml-auto mr-auto">
                      <p className="text-white text-center">Créez votre boîte</p>
                  </div>
                  <div className="w-full max-w-xs ml-auto mr-auto">
                      <form className="dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                          <div className="mb-4">
                              <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                  Nom de la boîte:
                              </label>
                              <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="name" type="text" onChange={
                                  (e: ChangeEvent<HTMLInputElement>) => {
                                      setBoxe({...boxe, name: e.target.value})
                                  }
                              } />
                          </div>
                          <PropMessage state={customOptions.state}/>
                          <div className="flex items-center justify-between mt-2">
                              <button className="w-full bg-blue-500 hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                                  Créer
                              </button>
                          </div>
                      </form>
                  </div>
              </div>
              <Footer/>
          </div>
      </PrivateRoute>
    )
}

export default CreateBoxes