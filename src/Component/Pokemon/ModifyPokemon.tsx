import PrivateRoute from "../../Router/PrivateRoute.tsx";
import {ChangeEvent, FormEvent, useEffect, useState} from "react";
import Message from "../Message/Message.tsx";
import Footer from "../Footer/Footer.tsx";
import Pokemon from "../../Entity/Interfaces/Pokemon.ts";
import GenderTypeCode from "../../Entity/GenderTypeCode.ts";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import {useLocation, useNavigate} from "react-router-dom";
import ConnectedHeader from "../Header/ConnectedHeader.tsx";
import CustomOption from "../../Entity/Interfaces/CustomOption.ts";

function ModifyPokemon(){

    const location = useLocation();

    const [pokemon, setPokemon] = useState<Pokemon>({
        id: location.state.pokemon.id,
        species: location.state.pokemon.species,
        name:location.state.pokemon.name,
        level: parseInt(location.state.pokemon.species),
        genderTypeCode: location.state.pokemon.genderTypeCode,
        isShiny: location.state.pokemon.isShiny,
        size: parseInt(location.state.pokemon.size),
        weight: parseInt(location.state.pokemon.weight),
        trainerId: parseInt(location.state.pokemon.trainerId),
        boxId: parseInt(location.state.pokemon.boxId)
    })

    const navigate = useNavigate()

    let customOptions: CustomOption = {
        state: {
            message: "",
            polarity: ""
        }
    }

    let options = {
        state : {
            message: "",
            polarity: ""
        },
        replace: false
    }

    const pokemonId = location.state.pokemon.id
    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        let route: string = ""

        fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("user-token")}`
            },
            body: JSON.stringify(pokemon)
        })
            .then( (response) => {

                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                else
                {
                    route = "/list-boxes"
                    options.state.message = `Le pokemon ${pokemon.name} a été modifier avec succès !`
                    options.state.polarity = "positive"
                    navigate(route, options)
                }
            })
            .catch((error) => {
                route = "/modify-pokemon"
                options.state.polarity = "negative"
                if (error.codeResponse === 400)
                {
                    options.state.message = "Un champs n'est pas remplis. Merci de remplir tous les champs avant d'essayer d'ajouter un pokemon."
                }
                if (error.codeResponse === 401) {
                    options.state.message = "Vous ne pouvez modifier ce pokemon car vous n'êtes pas authentifié. Merci de vous authentifié."
                }
                if (error.codeResponse === 403) {
                    options.state.message = "Vous ne pouvez modifier ce pokemon car vous n'avez pas les droits requis."
                }
                else
                {
                    options.state.message = "Le pokemon que vous essayé de modifier n'existe pas ou plus."
                }
                navigate(route, options)
            })
    }

    useEffect(() => {
        fetch(`http://localhost:8000/pokemons/${pokemonId}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                },
            })
            .then(async (response) => {
                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                const data = await response.json();
                setPokemon(data)
            })
            .catch((error) => {
                customOptions.state.polarity = "negative"
                if (error.codeStatus === 401)
                    customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                else
                    customOptions.state.message = "Le pokemon que vous essayé de chercher n'existe pas ou plus."

            })

    })
    return(
        <PrivateRoute defaultLocation="/sign-in">
            <div>
                <ConnectedHeader/>
                <div className="bg-blue-950 border-t-4 border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3 ml-auto mr-auto">
                    <p className="text-white text-center">Modifier le pokemon</p>
                </div>
                <div className="max-w-lg ml-auto mr-auto mt-5" id="add-pokemon-form">
                    <form className=" w-full dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="species">Espèce</label>
                            <input type="text" maxLength={12} defaultValue={location.state.pokemon.species} name="species" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, species: e.target.value})
                                }
                            }/>
                        </div>
                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="name">Nom</label>
                            <input type="text" maxLength={12} defaultValue={location.state.pokemon.name} name="name" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, name: e.target.value})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="level">Niveau</label>
                            <input type="number" min={0} max={100} defaultValue={location.state.pokemon.level} name="level" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, level: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="genderTypeCode">Genre</label>
                            <select name="genderTypeCode" defaultValue={location.state.pokemon.genderTypeCode} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLSelectElement>) => {
                                    switch (e.target.value) {
                                        case "MALE":
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.MALE})
                                            break;
                                        case "FEMALE":
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.FEMALE})
                                            break;
                                        default:
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.NOT_DEFINED})
                                            break;
                                    }
                                }
                            }>
                                <option value="MALE">Male</option>
                                <option value="FEMALE">Female</option>
                                <option value="NOT_DEFINED">Non définie</option>
                            </select>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="size">Taille</label>
                            <input type="number" min={1} defaultValue={location.state.pokemon.size} name="size" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, size: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="weight">Poids</label>
                            <input type="number" min={0.1} defaultValue={location.state.pokemon.weight} step="0.1" name="weight"  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, weight: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="isShiny">Est-il/elle lumineux ?</label>
                            <input type="checkbox" defaultChecked={location.state.pokemon.isShiny} name="isShiny" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, isShiny: e.target.checked})
                                }
                            }/>
                        </div>

                        <Message/>
                        <div className="flex items-center justify-between mt-5 mb-6">
                            <button className="w-full bg-blue-500 hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                                Modifier
                            </button>
                        </div>
                    </form>
                </div>
                <Footer/>
            </div>
        </PrivateRoute>
    )
}

export default ModifyPokemon