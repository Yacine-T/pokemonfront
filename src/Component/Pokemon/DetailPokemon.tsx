import PrivateRoute from "../../Router/PrivateRoute.tsx";
import ConnectedHeader from "../Header/ConnectedHeader.tsx";
import {useEffect, useState} from "react";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import Pokemon from "../../Entity/Interfaces/Pokemon.ts";
import GenderTypeCode from "../../Entity/GenderTypeCode.ts";
import {useLocation, useNavigate} from "react-router-dom";
import CustomOption from "../../Entity/Interfaces/CustomOption.ts";
import Footer from "../Footer/Footer.tsx";
import Trainer from "../../Entity/Interfaces/Trainer.ts";

function DetailPokemon() {

    const location = useLocation();
    const navigate = useNavigate()
    let customOptions: CustomOption = {
        state: {
            message: "",
            polarity: ""
        }
    }

    const pokemonInitializer: Pokemon = {
        id: null,
        species: "",
        name: "",
        level: -1,
        genderTypeCode: GenderTypeCode.NOT_DEFINED,
        isShiny: false,
        size: -1,
        weight: -1,
        trainerId: -1,
        boxId: -1
    }

    const trainerInitializer: Trainer = {
        id: null,
        firstName: "",
        lastName: "",
        birthDate: "",
        login: "",
        password: ""}

    const [pokemon, setPokemon] = useState(pokemonInitializer)

    const [trainer, setTrainer] = useState(trainerInitializer)

    const pokemonId = location.state.pokemonId

    useEffect(() => {
        fetch(`http://localhost:8000/pokemons/${pokemonId}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                },
            })
            .then(async (response) => {
                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                const data: Pokemon = await response.json();
                setPokemon(data)
            })
            .then(() => {
                const trainerId = pokemon.trainerId
                fetch(`http://localhost:8000/trainers/${trainerId}`,
                    {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                        },
                    }).
                then(async (response) => {
                    if (!response.ok)
                        throw new HttpErrorResponse(response.status)
                    const data: Trainer = await response.json();
                    setTrainer(data)
                })
            })
            .catch((error) => {
                customOptions.state.polarity = "negative"
                if (error.codeStatus === 401)
                    customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                else
                    customOptions.state.message = "Aucune dresseur n'a été retrouvé."

            })

    })
    return (
        <PrivateRoute defaultLocation="sign-in">
            <ConnectedHeader/>
            <div>
                <div
                    className="bg-blue-950 border-t-4 mr-auto ml-auto border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3">
                    <p className="text-white text-center"><strong>Pokemon: </strong> {pokemon.name}</p>
                </div>
                <div
                    className="bg-white border-t-4 shadow-xl mr-auto ml-auto border-blue-900 flex flex-col w-1/5 mt-5">
                    <div className="ml-5 mt-5 w-2/3 h-full w-fit">
                        <p className="w-fit"><strong>Espèce :</strong> {pokemon.species}</p>
                        <p className="w-fit"><strong>Name :</strong> {pokemon.name}</p>
                        <p className="w-fit"><strong>Niveau :</strong> {pokemon.level}</p>
                        <p className="w-full"><strong>Genre
                            :</strong> {pokemon.genderTypeCode === GenderTypeCode.MALE ? "Male" : pokemon.genderTypeCode === GenderTypeCode.FEMALE ? "Female" : "Non défnie"}
                        </p>
                        <p className="w-fit"><strong>Chromatique :</strong> {pokemon.isShiny ? "Oui" : "Non"}</p>
                        <p className="w-fit"><strong>Taille :</strong> {pokemon.size} cm</p>
                        <p className="w-fit"><strong>Poids :</strong> {pokemon.weight} kg</p>
                        <p className="w-fit"><strong>Dresseur :</strong> {trainer.login}</p>
                    </div>
                    <button type="button"
                            className="w-6/12 h-9 mr-auto ml-auto mt-5 mb-3 text-white bg-gray-900 hover:bg-green-500  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                            onClick={
                                () => {
                                    let options = {
                                        state: {
                                            pokemon: pokemon
                                        },
                                        replace: true
                                    }
                                    navigate("/modify-pokemon", options)
                                }
                            }>Modifier
                    </button>
                    <button type="button"
                            className="w-6/12 h-9 mr-auto ml-auto mb-5 text-white bg-red-500 hover:bg-red-800  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                            onClick={() => {
                                fetch(`http://localhost:8000/pokemons/${pokemonId}`,
                                    {
                                        method: "DELETE",
                                        headers: {
                                            "Content-Type": "application/json",
                                            "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                                        },
                                    })
                                    .then(async (response) => {
                                        if (!response.ok)
                                            throw new HttpErrorResponse(response.status)
                                        else {
                                            let options = {
                                                state: {
                                                    message: "Votre pokemon a été supprimé avec succès !",
                                                    polarity: "positive",
                                                    box: {
                                                        id: pokemon.boxId
                                                    }
                                                },
                                                replace: true
                                            }
                                            navigate("/detail-boxe", options)
                                        }
                                    })
                                    .catch((error) => {
                                        customOptions.state.polarity = "negative"
                                        if (error.codeStatus === 401)
                                            customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                                        else
                                            customOptions.state.message = "Vous n'êtes pas autorisé à supprimer ce pokemon."

                                    })

                            }}>
                        Supprimer
                    </button>
                </div>
            </div>
            <Footer/>
        </PrivateRoute>
    )
}

export default DetailPokemon