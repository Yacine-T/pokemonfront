interface Trainer {
    id: number | null
    firstName: String
    lastName: String
    login: String
    birthDate: String
    password: String
}

export default Trainer