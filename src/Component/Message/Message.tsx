import {useLocation} from "react-router-dom";

function Message() {
    const location = useLocation()
    const state = location.state
    const doesPropertyExist: boolean = state?.message !== undefined && state?.polarity !== undefined
    if(doesPropertyExist)
    {
        if(state.message.length > 0 && state.message.length > 0)
        {
            const classNameDiv= state.polarity === "negative" ?  "bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" : "bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md"
            const classNametext = state.polarity === "negative" ? "text-red text-center" : "text-green text-center"
            return (
                <div className={classNameDiv} role="alert">
                    <p className={classNametext}>{location.state.message}</p>
                </div>
            )
        }
    }
}

export default Message