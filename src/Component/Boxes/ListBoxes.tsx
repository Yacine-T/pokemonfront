import {useNavigate} from "react-router-dom";
import Footer from "../Footer/Footer.tsx";
import ConnectedHeader from "../Header/ConnectedHeader.tsx";
import {IoIosAdd} from "react-icons/io";
import PrivateRoute from "../../Router/PrivateRoute.tsx";
import {useEffect, useState} from "react";
import Boxe from "../../Entity/Interfaces/Boxe.ts";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import CustomOption from "../../Entity/Interfaces/CustomOption.ts";
import PropMessage from "../Message/PropMessage.tsx";
import Message from "../Message/Message.tsx";


function ListBoxes() {
    const navigate = useNavigate()
    const [boxes, setBoxes] = useState<Array<Boxe>>([])

    let customOptions: CustomOption = {
        state: {
            message: "",
            polarity: ""
        }
    }

    useEffect(() => {
        const trainerId = localStorage.getItem("trainer-id")
        fetch(`http://localhost:8000/trainers/${trainerId}/boxes`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("user-token")}`
                },
            })
            .then(async (response) => {
                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                const data: Array<Boxe> = await response.json();
                setBoxes(data)
            })
            .catch((error) => {
                customOptions.state.polarity = "negative"
                if (error.codeStatus === 401)
                    customOptions.state.message = "Vous ne pouvez pas consulter vos boîtes car vous n'avez pas été authentifié. Merci de vous authentifié."
                else
                    customOptions.state.message = "Aucune boîte vous appartenant n'a été retrouvée."

            })

    })

    const style = {
        display: "flex",
        justifyContent: "center"
    }

    return (
        <PrivateRoute defaultLocation="/sign-in">
            <div>
                <ConnectedHeader/>
                <div id="container" style={style}>
                    <div
                        className="bg-blue-950 border-t-4 border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3">
                        <p className="text-white text-center">Mes boîtes</p>
                    </div>
                    <div id="buttonContainer" className="mt-5 ml-5">
                        <button type="button"
                                className="text-white bg-gray-900 hover:bg-green-500 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                onClick={
                                    () => {
                                        navigate("/create-boxes")
                                    }
                                }>
                            <IoIosAdd className="w-14 h-14"/>
                        </button>
                    </div>
                </div>
                <div className="w-1/2 ml-auto mr-auto mt-3">
                    <Message/>
                </div>

                <div className="grid grid-cols-4 gap-4 mt-5 ml-5 mr-5">
                    {
                        boxes.map(boxe => {
                            return (

                                    <div className="bg-white border-t-4 shadow-xl border-blue-900 flex flex-col items-center">
                                        <div className="text-center">
                                            <p>{boxe.name}</p>
                                        </div>
                                        <button type="button"
                                                className="w-6/12 h-9 mt-5 mb-3 text-white bg-gray-900 hover:bg-green-500  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-14 h-14 focus:outline-none dark:focus:ring-blue-800"
                                                onClick={
                                                    () => {
                                                        const options = {
                                                            state: {
                                                                box: boxe,
                                                                trainerId: localStorage.getItem("trainer-id")
                                                            },
                                                            replace: true
                                                        }
                                                        navigate("/detail-boxe", options)
                                                    }
                                                }>Détail
                                        </button>
                                    </div>


                                )
                        })
                    }
                </div>

                <div className="w-1/2 ml-auto mr-auto mt-3">
                    <PropMessage state={customOptions.state}/>
                </div>
            </div>
            <Footer/>
        </PrivateRoute>
    )
}

export default ListBoxes