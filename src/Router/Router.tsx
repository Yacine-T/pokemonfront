import {createBrowserRouter} from "react-router-dom";
import App from "../App.tsx";
import SignIn from "../Component/SignIn/SignIn.tsx";
import SignUp from "../Component/SignUp/SignUp.tsx";
import About from "../Component/About/About.tsx";
import CreateBoxes from "../Component/Boxes/CreateBoxes.tsx";
import ListBoxes from "../Component/Boxes/ListBoxes.tsx";
import AddPokemon from "../Component/Pokemon/AddPokemon.tsx";
import DetailBoxe from "../Component/Boxes/DetailBoxe.tsx";
import DetailPokemon from "../Component/Pokemon/DetailPokemon.tsx";
import ModifyPokemon from "../Component/Pokemon/ModifyPokemon.tsx";

const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>
    },
    {
        path: "/sign-in",
        element: <SignIn/>
    },
    {
        path: "/sign-up",
        element: <SignUp/>
    },
    {
        path: "/about",
        element: <About/>
    },
    {
        path: "/create-boxes",
        element: <CreateBoxes/>
    },
    {
        path: "list-boxes",
        element: <ListBoxes/>
    },
    {
        path: "add-pokemon",
        element: <AddPokemon/>
    },
    {
        path: "detail-boxe",
        element: <DetailBoxe/>
    },
    {
        path: "detail-pokemon",
        element: <DetailPokemon/>
    },
    {
        path: "modify-pokemon",
        element: <ModifyPokemon/>
    }
    ])

export default router