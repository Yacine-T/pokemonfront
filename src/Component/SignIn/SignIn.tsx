import {ChangeEvent, FormEvent, useState} from "react";
import {useNavigate} from "react-router-dom";
import Message from "../Message/Message.tsx";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import NotConnectedHeader from "../Header/NotConnectedHeader.tsx";
import "../../assets/signin.css"
import Footer from "../Footer/Footer.tsx";

function SignIn(){

    let [trainer, setTrainer] = useState({firstName: "", lastName: "", birthDate: "", login: "", password: ""})
    const navigate = useNavigate()

    let options = {
        state: {
            message: "",
            polarity: ""
        },
        replace: false
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        let route: string = ""
        fetch("http://localhost:8000/login", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(trainer)
        }).then( (response) => {
            if (!response.ok)
                throw new HttpErrorResponse(response.status)
            else
            {
                return response.json()
            }
        })
            .then((data) => {
                route = "/list-boxes"
                localStorage.setItem("user-token", data.accessToken)
                localStorage.setItem("trainer-id", data.trainerId)
                navigate(route)
            })
            .catch( (error) => {
                route = "/sign-in"
                options.state.polarity = "negative"
                if (error.codeResponse === 400)
                {
                    options.state.message = "Certains champs ne sont pas remplis. Merci de remplir tous les champs avant d'essayer de vous connecter."
                }
                else
                {
                    options.state.message = "Le login ou le mot de passe saisi est incorrecte ."
                }
                navigate(route, options)
            })
    }

    return (

        <div>
            <NotConnectedHeader/>
            <div className="w-full max-w-xs" id="signin-form">
                <form className="dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="username">
                            Nom d'utilisateur
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="username" type="email" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, login: e.target.value})
                            }
                        } />
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="password">
                            Mot de passe
                        </label>
                        <input className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" name="password" type="password" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, password: e.target.value})
                            }
                        }/>
                            <Message/>
                    </div>
                    <div className="flex items-center justify-between">
                        <button className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                            Se connecter
                        </button>
                    </div>
                </form>
            </div>
            <Footer/>
        </div>

    );
}


export default SignIn;