import PrivateRoute from "../../Router/PrivateRoute.tsx";
import {ChangeEvent, FormEvent, useState} from "react";
import Message from "../Message/Message.tsx";
import Footer from "../Footer/Footer.tsx";
import Pokemon from "../../Entity/Interfaces/Pokemon.ts";
import GenderTypeCode from "../../Entity/GenderTypeCode.ts";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import {useNavigate} from "react-router-dom";
import ConnectedHeader from "../Header/ConnectedHeader.tsx";

function AddPokemon(){

    const trainerId: string | null = localStorage.getItem("trainer-id")

    const pokemonInitalizer: Pokemon = {
        id: null,
        species:"",
        name:"",
        level: -1,
        genderTypeCode: GenderTypeCode.NOT_DEFINED,
        isShiny: false,
        size: -1,
        weight: -1,
        trainerId: trainerId ? parseInt(trainerId) : -1,
        boxId: 4
    }

    const [pokemon, setPokemon] = useState(pokemonInitalizer)
    const navigate = useNavigate()

    let options = {
        state : {
            message: "",
            polarity: ""
        },
        replace: false
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        console.log(pokemon)
        let route: string = ""
        fetch("http://localhost:8000/pokemons", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("user-token")}`
            },
            body: JSON.stringify(pokemon)
        })
            .then( (response) => {

                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                else
                {
                    route = "/add-pokemon"
                    options.state.message = `Félicitation vous avez ajouter un pokemon à votre boîte !`
                    options.state.polarity = "positive"
                    navigate(route, options)
                }
            })
            .catch((error) => {
                route = "/add-pokemon"
                options.state.polarity = "negative"
                if (error.codeResponse === 400)
                {
                    options.state.message = "Un champs n'est pas remplis. Merci de remplir tous les champs avant d'essayer d'ajouter un pokemon."
                }
                else
                {
                    options.state.message = "Vous ne pouvez pas créer de boîte car vous n'avez pas été authentifié. Merci de vous authentifié."
                }
                navigate(route, options)
            })
    }

    return(
        <PrivateRoute defaultLocation="/sign-in">
            <div>
                <ConnectedHeader/>
                <div className="bg-blue-950 border-t-4 border-green-300 rounded-b text-teal-900 px-4 py-3 shadow-md mt-3 w-1/3 ml-auto mr-auto">
                    <p className="text-white text-center">Créer et ajouter un pokemon</p>
                </div>
                <div className="max-w-lg ml-auto mr-auto mt-5" id="add-pokemon-form">
                    <form className=" w-full dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="species">Espèce</label>
                            <input type="text" maxLength={12} name="species" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, species: e.target.value})
                                }
                            }/>
                        </div>
                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="name">Nom</label>
                            <input type="text" maxLength={12} name="name" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, name: e.target.value})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="level">Niveau</label>
                            <input type="number" min={0} max={100} name="level" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, level: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="genderTypeCode">Genre</label>
                            <select name="genderTypeCode" defaultValue="2" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLSelectElement>) => {
                                    switch (e.target.value) {
                                        case "MALE":
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.MALE})
                                            break;
                                        case "FEMALE":
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.FEMALE})
                                            break;
                                        default:
                                            setPokemon({...pokemon, genderTypeCode: GenderTypeCode.NOT_DEFINED})
                                            break;
                                    }
                                }
                            }>
                                <option value="MALE">Male</option>
                                <option value="FEMALE">Female</option>
                                <option value="NOT_DEFINED">Non définie</option>
                            </select>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="size">Taille</label>
                            <input type="number" name="size" min={1} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, size: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="weight">Poids</label>
                            <input type="number" min={0.1} step="0.1" name="weight"  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, weight: parseInt(e.target.value)})
                                }
                            }/>
                        </div>

                        <div className="mb-6">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="isShiny">Est-il/elle lumineux ?</label>
                            <input type="checkbox" name="isShiny" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" onChange={
                                (e: ChangeEvent<HTMLInputElement>) => {
                                    setPokemon({...pokemon, isShiny: e.target.checked})
                                }
                            }/>
                        </div>

                        <Message/>
                        <div className="flex items-center justify-between mt-5 mb-6">
                            <button className="w-full bg-blue-500 hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                                Créer
                            </button>
                        </div>
                    </form>
                </div>
                <Footer/>
            </div>
        </PrivateRoute>
    )
}

export default AddPokemon