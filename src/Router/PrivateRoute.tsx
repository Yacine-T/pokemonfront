import {Navigate} from "react-router-dom";
import RouterRedirector from "../Entity/Interfaces/RouterRedirector.ts";

function PrivateRoute(routerRedirector: RouterRedirector) {
    const isLoggedIn = !!localStorage.getItem("user-token")
    if (isLoggedIn)
    {
        return routerRedirector.children
    }
    return <Navigate to={routerRedirector.defaultLocation}/>
}

export default PrivateRoute