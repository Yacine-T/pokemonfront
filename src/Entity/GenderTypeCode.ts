enum GenderTypeCode {
    MALE= "MALE",
    FEMALE= "FEMALE",
    NOT_DEFINED= "NOT_DEFINED"
}

export default GenderTypeCode