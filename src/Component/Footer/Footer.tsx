import {NavLink} from "react-router-dom";
import "../../assets/footer.css"

function Footer() {
    return(
        <footer className=" bg-blue-950 footer footer-center p-4 bg-base-300 text-base-content">
            <aside>
                <NavLink className="text-white md:dark:hover:text-blue-500" to="/about">A propos</NavLink>
            </aside>
        </footer>
    )
}

export default Footer