import GenderTypeCode from "../GenderTypeCode.ts";

interface Pokemon {
    id:number | null
    species:string
    name:string
    level:number
    genderTypeCode: GenderTypeCode
    isShiny:boolean
    size:number
    weight:number
    trainerId:number
    boxId:number
}

export default Pokemon