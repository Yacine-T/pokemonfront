import pokemonLogo from "../../assets/images/pokemon_logo.svg";
import {NavLink} from "react-router-dom";

const ConnectedHeader = () => {
    return (

        <nav className="border-gray-200 dark:bg-gray-900 dark:border-gray-700">
            <div className="flex flex-wrap items-center justify-between mx-auto p-4">
                <img className="h-14" src={pokemonLogo} alt="pokemon_logo"/>
                <ul className="flex flex-col font-medium p-4 md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:dark:bg-gray-900">
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/list-boxes">Mes boîtes</NavLink>
                    </li>
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/my-exchanges">Mes échanges</NavLink>
                    </li>
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/search-for-trainers">Chercher un(e) Dresseur(euse)s</NavLink>
                    </li>
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/search-for-pokemon">Chercher un pokemon</NavLink>
                    </li>
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/user-profils">Profil utilisateur(ice)s</NavLink>
                    </li>
                    <li>
                        <NavLink className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent" to="/sign-in"
                         onClick={() => {
                            localStorage.clear()
                        }}>Déconnexion</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default ConnectedHeader