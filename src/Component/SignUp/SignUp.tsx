import {ChangeEvent, FormEvent, useState} from "react";
import Trainer from "../../Entity/Interfaces/Trainer.ts";
import {useNavigate} from "react-router-dom";
import Message from "../Message/Message.tsx";
import HttpErrorResponse from "../../Entity/Classes/HttpErrorResponse.ts";
import NotConnectedHeader from "../Header/NotConnectedHeader.tsx";
import Footer from "../Footer/Footer.tsx";
import "../../assets/signup.css"

function SignUp(){

    let [trainer, setTrainer] = useState<Trainer>({id: null, firstName: "", lastName: "", login: "", birthDate: "", password: ""})
    const navigate = useNavigate()
    const currentDate: string = new Date().toISOString().split("T")[0]

    let options = {
        state : {
            message: "",
            polarity: "",
        },
        replace: false
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        let route: string = ""
        fetch("http://localhost:8000/subscribe", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(trainer)
        })
            .then( (response) => {

                if (!response.ok)
                    throw new HttpErrorResponse(response.status)
                else
                {
                    route = "/sign-in"
                    options.state.message = `Félicitation ${trainer.firstName}. Votre inscription a été prise en compte !`
                    options.state.polarity = "positive"
                    navigate(route, options)
                }
            })
            .catch( (error) => {
                route = "/sign-up"
                options.state.polarity = "negative"
                if (error.codeResponse === 400)
                {
                    options.state.message = "Certains champs ne sont pas remplis. Merci de remplir tous les champs avant d'essayer de vous inscrire."
                }
                else
                {
                    options.state.message = "Le login que vous avez soumis est déjà utilisé."
                }
                navigate(route, options)
            })
    }

    return(

        <div>
            <NotConnectedHeader/>
            <div className="w-full max-w-xs" id="signin-form">
                <form className="dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="firstName">Prénom</label>
                        <input type="text" name="firstName" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, firstName: e.target.value})
                            }
                        }/>
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="lastName">Nom de famille</label>
                        <input type="text" name="lastName" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, lastName: e.target.value})
                            }
                        }/>
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="birthDate">Date de naissance</label>
                        <input type="date" name="birthDate" max={currentDate} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, birthDate: e.target.value})
                            }
                        }/>
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="login">Nom d'utilisateur</label>
                        <input type="email" name="login" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, login: e.target.value})
                            }
                        }/>
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="password">Mot de passe</label>
                        <input type="password" name="password" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" onChange={
                            (e: ChangeEvent<HTMLInputElement>) => {
                                setTrainer({...trainer, password: e.target.value})
                            }
                        }/>
                    </div>
                    <Message/>
                    <div className="flex items-center justify-between mb-6">
                        <button className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                            S'incrire
                        </button>
                    </div>
                </form>
            </div>
            <Footer/>
        </div>

    )
}

export default SignUp;