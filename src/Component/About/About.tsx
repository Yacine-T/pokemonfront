import NotConnectedHeader from "../Header/NotConnectedHeader.tsx";
import Footer from "../Footer/Footer.tsx";

function About() {
    return (
        <div>
            <NotConnectedHeader/>
            <div>
                <p>Application créee par <a href="https://www.linkedin.com/in/yacinetazdait/?originalSubdomain=fr">Yacine Tazdait</a></p>
            </div>
            <Footer/>
        </div>
    )
}

export default About