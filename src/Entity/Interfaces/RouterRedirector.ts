import {ReactNode} from "react";

interface RouterRedirector{
    defaultLocation: string
    children: ReactNode
}

export default RouterRedirector