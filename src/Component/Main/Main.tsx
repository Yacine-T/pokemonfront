import PrivateRoute from "../../Router/PrivateRoute.tsx";
import ListBoxes from "../Boxes/ListBoxes.tsx";
function Main() {
    return (
        <div>
            <PrivateRoute defaultLocation="/sign-in">
                <ListBoxes/>
            </PrivateRoute>
        </div>
    )
}

export default Main